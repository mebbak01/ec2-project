resource "aws_instance" "webserver" {
  count                  = var.counts
  key_name               = var.key_pair
  monitoring             = var.monitoring
  ami                    = "ami-0a91cd140a1fc148a"
  instance_type          = var.instance_type
  subnet_id              = "subnet-ee2069a2"

  user_data = <<-EOF
	      	#! /bin/bash
          sudo apt-get update
	       	sudo apt-get install -y apache2
	      	sudo systemctl start apache2
	      	sudo systemctl enable apache2
	      	echo "<h1>Deployed via Terraform</h1>" | sudo tee /var/www/html/index.html
	EOF

  tags = {
    Name = var.tag
  }



ebs_block_device {
    device_name = "/dev/sda1"
    volume_type = "gp2"
    volume_size = 30
 }
}

