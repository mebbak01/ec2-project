variable "region" {
    type        = string
    description = "aws region type"
    default     = "us-east-2"
}
variable "counts" {
    description = "number of instance you want to provision"
    default     = 1
}
variable "key_pair" {
    type  = string
    default = "Key1"
}
variable "tag" {
    type = string
    default = "webserver"
}
variable "monitoring" {
    default = true
}
variable "instance_type" {
    default = "t2.micro"
}
